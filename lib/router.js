'use strict';

const Error = require('@scola/error');
const EventHandler = require('@scola/events');
const Deep = require('@scola/deep');

class Router extends EventHandler {
  constructor(routes, serial, window) {
    super();

    this.routes = routes;
    this.window = window;

    this.stateProcessor = serial
      .setProcessor(this.processState.bind(this));

    this.viewDispatcher = null;

    this.defaults = new Map();
    this.targets = new Map();
    this.scripts = new Map();
    this.current = new Map();
  }

  addRoutes(routes) {
    Deep.assign(this.routes, routes);
    return this;
  }

  getViewDispatcher() {
    return this.viewDispatcher;
  }

  setViewDispatcher(viewDispatcher) {
    this.viewDispatcher = viewDispatcher;
    return this;
  }

  setDefault(name, route) {
    this.defaults.set(name, route);
    return this;
  }

  setTarget(name, target) {
    this.targets.set(name, target);
    return this;
  }

  setScript(name, file) {
    this.scripts.set(name, {
      file,
      loaded: false
    });

    return this;
  }

  start() {
    this.emit('debug', this, 'start');

    this.addHandlers();
    this.handleState();

    return this;
  }

  stop() {
    this.emit('debug', this, 'stop');
    this.removeHandlers();

    return this;
  }

  route(path, push) {
    this.emit('debug', this, 'route', path, push);

    const next = this.getDefinition(path);
    const current = this.current.get(next.target);

    if (current && next.route === current.route) {
      return Promise.resolve(this);
    }

    return this
      .loadScript(next.namespace)
      .then(this.handleLoadScript.bind(this, next, push))
      .catch(this.handleError.bind(this));
  }

  destroy(path) {
    this.emit('debug', this, 'destroy', path);

    const definition = this.getDefinition(path);
    this.getRoute(definition).destroy();

    return this.remove(path, false);
  }

  remove(path, push) {
    this.emit('debug', this, 'remove', path, push);

    const definition = this.getDefinition(path);
    this.current.delete(definition.target);

    if (push !== false) {
      this.pushState();
    }

    return this;
  }

  addHandlers() {
    this.bindListener('popstate', this.window, this.handleState);
  }

  removeHandlers() {
    this.unbindListener('popstate', this.window, this.handleState);
  }

  handleError(error) {
    this.emit('error', error);
  }

  loadScript(namespace) {
    this.emit('debug', this, 'loadScript', namespace);

    return new Promise((resolve, reject) => {
      const script = this.scripts.get(namespace);

      if (!script || script.loaded) {
        resolve();
        return;
      }

      const element = document.createElement('script');
      element.setAttribute('type', 'text/javascript');
      element.setAttribute('src', script.file);

      element.onload = () => {
        script.loaded = true;
        this.scripts.set(namespace, script);
        resolve();
      };

      element.onerror = () => {
        reject(new Error('router_script_not_loaded', {
          detail: {
            namespace,
            script
          }
        }));
      };

      document.head.appendChild(element);
    });
  }

  handleLoadScript(next, push) {
    this.emit('debug', this, 'handleLoadScript', next, push);

    const current = this.current.get(next.target);
    const route = this.getRoute(next);

    switch (this.getDirection(current, next)) {
      case 'forward':
        route.forward(next.parameters);
        break;
      case 'backward':
        route.backward(next.parameters);
        break;
      case 'immediate':
        route.immediate(next.parameters);
        break;
    }

    this.current.set(next.target, next);

    if (push !== false) {
      this.pushState();
    }

    return this;
  }

  handleState() {
    this.emit('debug', this, 'handleState');

    const routes = new Map();
    const destroy = new Map();

    this.window.location.hash.substr(2).split('/').forEach((path) => {
      const definition = this.getDefinition(path);

      if (definition &&
        this.routes[definition.route] ||
        this.scripts.has(definition.namespace)) {
        routes.set(definition.target, path);
      }
    });

    this.current.forEach((current) => {
      if (!routes.has(current.target)) {
        destroy.set(current.target, current.path);
      }
    });

    this.defaults.forEach((path, target) => {
      if (!routes.has(target)) {
        routes.set(target, path);
        destroy.delete(target);
      }
    });

    destroy.forEach((path) => {
      this.destroy(path);
    });

    this.stateProcessor
      .process([...routes.values()])
      .then(this.pushState.bind(this))
      .catch(this.handleError.bind(this));
  }

  processState(path) {
    this.emit('debug', this, 'processState', path);
    return this.route(path, false);
  }

  pushState() {
    this.emit('debug', this, 'pushState');

    const state = this.stringify();

    if (state !== this.window.location.hash.substr(1)) {
      this.window.history.pushState(state, null, '#' + state);
    }
  }

  getDirection(current, next) {
    this.emit('debug', this, 'getDirection', current, next);

    if (!current || Math.abs(current.name.length - next.name.length) > 1) {
      return 'immediate';
    }

    if (this.contains(next.name, current.name)) {
      return 'forward';
    } else if (this.contains(current.name, next.name)) {
      return 'backward';
    }

    return 'immediate';
  }

  getDefinition(path) {
    this.emit('debug', this, 'getDefinition', path);

    const parts = path.split('@');
    const [route] = path.split(':');
    const [nameString, parameters] = (parts[1] || parts[0]).split(':');
    const name = nameString.split('.');
    const namespace = name.slice(0, 2).join('.');
    const target = parts[1] ? parts[0] : namespace;

    return {
      name,
      namespace,
      parameters,
      path,
      route,
      target
    };
  }

  getRoute(definition) {
    this.emit('debug', this, 'getRoute', definition);

    return this.routes[definition.route]
      .get()
      .setViewDispatcher(this.viewDispatcher)
      .setRouter(this)
      .setTarget(this.targets.get(definition.target));
  }

  contains(outer, inner) {
    this.emit('debug', this, 'contains', outer, inner);

    return inner && outer ?
      outer.slice(0, inner.length).join() === inner.join() :
      false;
  }

  stringify() {
    this.emit('debug', this, 'stringify');

    let result = '';

    this.current.forEach((current) => {
      result += '/' + current.path;
    });

    return result;
  }
}

module.exports = Router;
