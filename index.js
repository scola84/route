'use strict';

const Async = require('@scola/async');
const DI = require('@scola/di');

const Abstract = require('./lib/abstract');
const Router = require('./lib/router');

class Module extends DI.Module {
  configure() {
    this.inject(Router).with(
      this.object({}),
      this.instance(Async.Serial)
    );

    if (typeof window !== 'undefined') {
      this.inject(Router)
        .insertArgument(2, this.value(window));
    }
  }
}

module.exports = {
  Abstract,
  Module,
  Router
};
